# postgresql-logger

Service to track changed of one table in an additional table.

## Installation

Just execute *install.sql* file. All necessary actions will be made and postgresql-logger will be ready to use.
 
## Uninstallation

Just execute *uninstall.sql* file. All necessary actions will be made and postgresql-logger with all own relations will be removed.
 
## Usage

All control actions are making with ordinary inserting, updating and deleting data from or to *logger* table.

### Start tracking

In order to start tracking a some table you should **INSERT** corresponding row into *logger* table.

| Column | Function |
|---|---|
| table_name | Table name to track  |
| enabled | Status of tracking  |
| primary_key | Primary keys that will separate columns in log table  |
| crucial_columns | Columns to add as separate columns |
| ignored_columns | Columns to not add to diff jsonb column |

After inserting a special trigger will create a table with name **{table_name}_log** and triggers that will be executed after each update query and add diff data to **{table_name}_log**.

### Pause and resume tracking

In order to pause or resume tracking the table you should **UPDATE** the **enabled** column of corresponding row.

After updating a special trigger will drop the tracking trigger for the table;

### Stop tracking and delete logs

In order to stop tracking the table you should **DELETE** the corresponding row. 

After deleting a special trigger will drop the tracking trigger for the table if it still exists and drop log table **{table_name}_log**. 

