--Use triggered logger_apply() to drop additional tables and triggers for the tracking tables;
DELETE FROM LOGGER;

--Drop Logger table with triggers;
DROP TABLE logger;

--Drop logger_apply() funxtion;
DROP FUNCTION IF EXISTS logger_apply();
