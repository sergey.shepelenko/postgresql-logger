--Logger table, a main control table. Insert/Update/Delete changes triggers corresponding actions;
CREATE TABLE logger
(
  table_name text NOT NULL,
  enabled boolean NOT NULL DEFAULT false,
  primary_key text[] NOT NULL DEFAULT ARRAY['id'::text],
  crucial_columns text[] NOT NULL DEFAULT ARRAY[]::text[],
  ignored_columns text[] NOT NULL DEFAULT ARRAY['created_at'::text, 'updated_at'::text],
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  CONSTRAINT logger_pkey PRIMARY KEY (table_name)
);
COMMENT ON COLUMN public.logger.table_name IS 'Table name to track';
COMMENT ON COLUMN public.logger.enabled IS 'Status of tracking';
COMMENT ON COLUMN public.logger.primary_key IS 'Primary keys that will separate columns in log table';
COMMENT ON COLUMN public.logger.crucial_columns IS 'Columns to add as separate columns';
COMMENT ON COLUMN public.logger.ignored_columns IS 'Columns to not add to diff jsonb column';

--Main working function that creates and drop log tables and tracking triggers;
CREATE OR REPLACE FUNCTION logger_apply()
  RETURNS trigger AS
$$
declare
  table_name text DEFAULT '';
  column_name text DEFAULT '';
  key_columns_text text DEFAULT '';
  other_columns_text text DEFAULT '';

  table_other_columns_text text DEFAULT '';
  trigger_key_columns_text text DEFAULT '';
  trigger_other_columns_text text DEFAULT '';

  ignored_column_names text[] DEFAULT null;
  ignored_columns_text text DEFAULT '''''';
  query text;
  enabe_trigger boolean DEFAULT null;
begin
  RAISE NOTICE 'processing %', TG_OP;

  CASE TG_OP
  WHEN 'INSERT' THEN
    table_name := new.table_name;

    FOREACH column_name IN ARRAY new.primary_key
    LOOP
      key_columns_text := key_columns_text || quote_ident(column_name) || ', ';
      trigger_key_columns_text := trigger_key_columns_text || 'new.' || quote_ident(column_name) || ', ';
    END LOOP;

    FOREACH column_name IN ARRAY new.crucial_columns
    LOOP
      other_columns_text := other_columns_text ||
        quote_ident('old_' || column_name) || ', ' ||
        quote_ident('new_' || column_name) || ', ';
      table_other_columns_text := table_other_columns_text ||
        quote_ident(column_name) || ' as ' || quote_ident('old_' || column_name) || ', ' ||
        quote_ident(column_name) || ' as ' || quote_ident('new_' || column_name) || ', ';
      trigger_other_columns_text := trigger_other_columns_text ||
        'old.' || quote_ident(column_name) || ', ' ||
        'new.' || quote_ident(column_name) || ', ';
    END LOOP;

    if array_length(new.ignored_columns, 1) != 0 then
      FOREACH column_name IN ARRAY new.ignored_columns
      LOOP
        ignored_column_names := ignored_column_names || quote_literal(column_name);
      END LOOP;
      ignored_columns_text = array_to_string(ignored_column_names, ', ');
    end if;

    query := format(
      'CREATE TABLE %I as
        select
          %s %s ''[]''::jsonb as diff
        from
          %I
        limit
          0;
      ALTER TABLE %I add ts timestamp with time zone DEFAULT now();',
      table_name || '_log',
      key_columns_text,
      table_other_columns_text,
      table_name,
      table_name || '_log'
    );

    RAISE NOTICE '%', query;
    EXECUTE query;

    query := format(
      'CREATE OR REPLACE FUNCTION %I()
          RETURNS trigger AS
        $BODY$
        declare
          info jsonb;
        begin
          info := (
	        select
	          jsonb_agg(jsonb_build_object(
	            ''key'', old_t.key,
	            ''old_value'', old_t.value,
	            ''new_value'', new_t.value
	          ))
	        from
		        (
			        select
			          t.key,
			          t.value
			        from
			          json_each(row_to_json(old)) t
		        ) as old_t
		        join (
			        select
			          t.key,
			          t.value
			        from
			          json_each(row_to_json(new)) t
		        ) as new_t
		        on old_t.key = new_t.key
	        where
	          old_t.value::text != new_t.value::text
	          and old_t.key not in (%s)
          );

          if info is null then
            info := ''[]''::jsonb;
          end if;

          insert into %I (%s %s diff) values (%s %s info);

          return new;
        end;
        $BODY$
      LANGUAGE plpgsql VOLATILE;',
      'log_' || table_name || '_change',
      ignored_columns_text,
      table_name || '_log',
      key_columns_text,
      other_columns_text,
      trigger_key_columns_text,
      trigger_other_columns_text
    );

    RAISE NOTICE '%', query;
    EXECUTE query;

  WHEN 'UPDATE' THEN
    table_name := new.table_name;
    if new.enabled != old.enabled then
      enabe_trigger := new.enabled;
    end if;

  WHEN 'DELETE' THEN
    table_name := old.table_name;
    enabe_trigger := false;
  END CASE;

  if enabe_trigger is not null  then
    query := format(
      'DROP TRIGGER IF EXISTS %I ON %I;',
      'log_' || table_name || '_change',
      table_name
    );
    RAISE NOTICE '%', query;
    EXECUTE query;

    if enabe_trigger then
      query := format(
        'CREATE TRIGGER %I
          AFTER UPDATE
          ON %I
          FOR EACH ROW
        EXECUTE PROCEDURE %I();',
        'log_' || table_name || '_change',
        table_name,
        'log_' || table_name || '_change'
      );
      RAISE NOTICE '%', query;
      EXECUTE query;
    end if;
  end if;

  if TG_OP != 'DELETE' then
    return new;
  else

    query := format(
      'DROP FUNCTION IF EXISTS %I();',
      'log_' || table_name || '_change'
    );
    RAISE NOTICE '%', query;
    EXECUTE query;

    query := format(
      'DROP TABLE IF EXISTS %I;',
      table_name || '_log'
    );
    RAISE NOTICE '%', query;
    EXECUTE query;

    return old;
  end if;
end;
$$
LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER logger_after_insert
  AFTER INSERT
  ON logger
  FOR EACH ROW
EXECUTE PROCEDURE logger_apply();

CREATE TRIGGER logger_after_update
  AFTER UPDATE
  ON logger
  FOR EACH ROW
EXECUTE PROCEDURE logger_apply();

CREATE TRIGGER logger_after_delete
  AFTER DELETE
  ON logger
  FOR EACH ROW
EXECUTE PROCEDURE logger_apply();
